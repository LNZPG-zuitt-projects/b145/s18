// count series of number input by the user
// 

// WHILE LOOP
function countUsingWhile(){
	let input1 =  document.getElementById('task1').value;
	if (input1 <= 0) {
		// not valid
		let msg = document.getElementById('message');
		msg.innerHTML = 'Value Not Valid';
	} else {
		// valid
		while (input1 !== 0){
		alert(input1);
		input1--
		}
	}
}



// DO-WHILE LOOP
function countUsingDoWhile(){
	// get input
	let number = document.getElementById('task2').value;
	// alert(number);
	// make sure value is valid
	if (number <= 0) {
			//not Valid
			// inform user that they cannot proceed.
			// using DOM selector
			let displayText = document.getElementById('info');
			displayText.innerHTML = "Not Valid Ghorl"

	} else {
			// valid
			// alert(number);
			// syntax:
			// 	do {
			// 		// body of the loop
			// 	} while (condition)
			//task: count from 1 to n (value by user)
				let indexStart = 1;
				let displayText = document.getElementById('info');
				displayText.innerHTML = number + ' is valid';
			do{
				alert('count: ' + indexStart);
				indexStart++
			} while (indexStart <= number);
	}
}



// FOR LOOP
// initialize
// condition
// iteration/final expression

function countUsingForLoop() {
	let data = document.getElementById('task3').value;
	let res =	document.getElementById('response');
	// validate if this is wahat we desire

	if (data <= 0) {
		res.innerHTML = 'Input Invalid';
	} else {
			for (let startCount = 0; startCount <= data; startCount++) {
					// since start counter strated with zero'0', every iteration should add value of 1 to meet condition and terminate process
					// descibe what will happer per iteration
					alert('This is the value in this iteration: '+ startCount);
			}
	}
}



// Access elements of a string using repetition control strctures



function accessElementsInString(){
	let name = document.getElementById('userName').value;
	// alert(name);

	let textLength = document.getElementById('stringLength');

	if (name !== '') {
		// response if truthy
		// analyze value inserted by user
			// 1. determine length of string
				// => invoke the "length" property of a string using (.) notation.
			textLength.innerHTML = 'The string is ' + name.length + ' characters long.';

			// uppon accessing elements inside a string, this can be done so, using by [] square brackets'
			// we can access each element through the use of it's index number/count
			// count will start at '0'
			// console.log(name[0]);
			// console.log(name[1]);
			// console.log(name[2]);
			// console.log(name[3]);
			// console.log(name[4]);
			// console.log(name[5]);
			// console.log(name[6]);


		// initialize
		// condition
		// iteration/final expression
			for (let startIndex = 0; startIndex < name.length; startIndex++) {
			// access each element

			console.log(name[startIndex]);
			}
	} else {
		// reponse if falsy
		alert('Value is Invalid');
	}
}



// BEHAVIOUR: if the string provided is an odd number, the middle  character does not need to be checked.
	// d a d //palindrome
	// k a y a k //palindrome

//We will create a loop through half of the string's characters that checks if the letters at the front and back of the string is the same

// DETECT PALINDROME
function detectPalindrome(){
	// 1. get input using DOM slectors
	let word = document.getElementById('word').value;
	let response = document.getElementById('detectPalindrome');
	// alert(word); //checker

	// 2. Validate the data if it is indeed the correct type of info.

	if (word !== '') {
		// identify how long the word is
		let wrdLength = word.length;
		for (let index = 0; index < wrdLength / 2; index++) {
			// instruction that will upon each iteration of the loop
			//  we are trying to get the current element in the string acccording to the index
			// get last element of the string by deducting 1 in current length of string since index starts with 0
		if (word[index] !== word[wrdLength - 1 - index]){
		// response
		response.innerHTML = word + '<h3 class="text-primary">is Not a Palindrome</h3>';
		} else {
		// response
		console.log(word[index] +  ' is the same as ' +  word[wrdLength -1 - index]);
		response.innerHTML = word + '<h3 class="text-success">is a Palindrome</h3>';

		}
		}
	} else {
				response.innerHTML = '<h3 class="text-danger">is Not Valid</h3>';
	}
}




function getOddNumbers(){
	// target the value of the input field
	let inputCount = document.getElementById('value4').value;

	let res = document.getElementById('getOddNum')
	// alert(inputCount);

	// validate data to make sure only get positive numbers
	if (inputCount > 0) {
			// response if pass

			// create a loop that will produce the set of numbers depending on the input of the user
			for (let count = 0; count <= inputCount; count++) {
			//lets create another logic that will tweak out the even numbers away from tthe set of numbers
			if (count % 2 === 0) {
					// even number
					continue; 
					}
			// print series of numbers inside console
				console.log(count);
			}

			// res.innerHTML = '<h3 class="text-warning"> Proceed </h3>'
	} else {
			// response if fail
 			res.innerHTML = '<h3 class="text-danger"> Number should be greater than 0 </h3>'
	}
}

//modify the code above to return a series of numbers and display inside the console without the even numbers.
//activity: commit -m :done with activity
//screen grab: in hangouts